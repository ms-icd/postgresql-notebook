# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
ARG BASE_CONTAINER=jupyter/minimal-notebook
FROM $BASE_CONTAINER

LABEL maintainer="Emmnanuel Braux <emmanuel.braux@imt-atlantique.fr>"

USER root

# postgres 
RUN apt-get update && apt-get install -y ca-certificates wget lsb-release gnupg &&\
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - &&\
    echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/PostgreSQL.list && \
    apt-get update && \
    apt-get install -y  postgresql-10 postgresql-contrib && \
    rm -rf /var/lib/apt/lists/*


RUN echo "jovyan ALL=(postgres) NOPASSWD: ALL" >> /etc/sudoers
RUN chmod 777 /var/run/postgresql


USER $NB_UID

# Install Python 3 packages
RUN conda install --quiet --yes \
#    'beautifulsoup4=4.8.*' \
#    'conda-forge::blas=*=openblas' \
#    'bokeh=1.4.*' \
#    'cloudpickle=1.3.*' \
    'sqlalchemy=1.3.*' \
    'ipython-sql' \
    'psycopg2' \
#    'pprint' \
    'pgspecial' \
    && \
    conda clean --all -f -y && \
    #npm cache clean --force && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER



USER $NB_UID

# Init env pour postgres et sa BDD locale
RUN echo '# Répertoire données de PostgreSQL \n\
export PGDATA=$HOME/work/.postgreSQL_datas' >> $HOME/.bashrc

RUN echo '# Ajout au PATH des exécutables PostgreSQL \n\
export PATH=/usr/lib/postgresql/10/bin/:$PATH' >> $HOME/.bashrc

USER root

COPY entrypoint.sh /srv/entrypoint.sh
RUN chown -R jovyan:users /srv/entrypoint.sh && \
    chmod +x /srv/entrypoint.sh

COPY initialisation.sh /srv/initialisation.sh
RUN chown -R jovyan:users /srv/initialisation.sh && \
    chmod +x /srv/initialisation.sh

COPY install_database_soins.sql /srv/install_database_soins.sql
RUN chown -R jovyan:users /srv/install_database_soins.sql


COPY notebooks latest/notebooks/
RUN chown -R jovyan:users latest/notebooks/ 

RUN chown -R jovyan:users latest && \
    find latest -type f -iname "*" -print0 | xargs -I {} -0 chmod 0444 {} && \
    find latest -type d -iname "*" -print0 | xargs -I {} -0 chmod 0544 {}


COPY notebooks work/notebooks/
RUN chown -R jovyan:users work/notebooks/



USER $NB_UID
ENTRYPOINT ["tini", "--", "/srv/entrypoint.sh"]
#CMD ["start-singleuser.sh"]
#ENTRYPOINT ["tini", "-g", "--"]
CMD ["start-notebook.sh"]


# changement d'utilisateur pour sécuriser
USER $NB_UID

WORKDIR ${HOME}/work
