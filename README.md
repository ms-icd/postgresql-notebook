

# Image Docker Jupyter Notebook + posgresql

## Lancement du notebook

Préparation du dossier pour la persistance des donnees
``` bash
cd 
mkdir bd_datas
chmod 777 bd_datas
```

Lancement du notebook
``` bash
cd bd_datas
docker  run --rm -p 8888:8888 -v ${PWD}:/home/jovyan/work registry.gitlab.com/ms-icd/postgresql-notebook
``` 
Le lien de connexion apparait sur la console, au format : `http://127.0.0.1:8888/?token=30cfab070e79cdfe0381efb9138d113fd57506927bdd6ca2`

Pour arrêter le notebook, `<CTRL-C>`.

* L'option -p permet d'accèder au serveur jupyter dans le container
* L'option -rm supprime le container après execution
* L'option -v permet de conserver les donneées dans le dossier local, et de les retrouver quand on relance le container


## Réinitialisation de la base de données

``` bash
docker  run --rm -v ${PWD}:/home/jovyan/work registry.gitlab.com/ms-icd/postgresql-notebook rm -rf /home/jovyan/work/.postgreSQL_datas
```

## Ménage des données une fois le TP terminé

``` bash
docker  run --rm -v ${PWD}:/home/jovyan/work registry.gitlab.com/ms-icd/postgresql-notebook find . -name . -o -prune -exec rm -rf -- {} +
```

## Si besoin, build de l'image en local 

Recuperation du dépot
``` bash
git clone https://gitlab.com/ms-icd/postgresql-notebook.git
```

Build de l'image
``` bash
docker build -t postgresql-notebook postgresql-notebook
```

Vérification
``` bash
docker images 
```

Pour utiliser l'imge buildée en local, dans les commandes `docker run`, remplacer `registry.gitlab.com/ms-icd/postgresql-notebook` par `postgresql-notebook`


## TODO

Mieux gérer les droits, pour éviter que les données soient créées avec un ID interne à l'image, imposant le `chmod 777` à la création du dossier

